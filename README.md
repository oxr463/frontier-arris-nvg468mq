# Frontier Arris NVG468MQ

[![unlicense][unlicense-badge]][unlicense-ref]
[![cc-zero][cc-zero-badge]][cc-zero-ref]

## License

All source code and accompanying documentation has been dedicated to the public domain.

[cc-zero-badge]: https://img.shields.io/badge/license-CC0-black.svg?style=flat-square
[cc-zero-ref]: LICENSE.txt
[unlicense-badge]: https://img.shields.io/badge/license-public--domain-black.svg?style=flat-square
[unlicense-ref]: UNLICENSE
